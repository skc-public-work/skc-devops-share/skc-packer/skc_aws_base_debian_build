




skc_ami_name      = "skc-base-debian-11-00"
skc_instance_type = "t2.micro"
skc_source_ami    = "ami-0c7c4e3c6b4941f0f"
skc_ssh_username  = "admin"
skc_region        = "us-east-2"
skc_vpc_id        = "vpc-d7589ebc"
skc_subnet_id     = "subnet-005077c903d924916"
skc_tag_one       = "skc-bade-deb-11"
