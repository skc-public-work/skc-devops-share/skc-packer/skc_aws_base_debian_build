

variable "aws_access_key" {
  type    = string
  default = env("AWS_ACCESS_KEY_ID")
}

variable "aws_secret_access_key" {
  type    = string
  default = env("AWS_SECRET_ACCESS_KEY")
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}


variable "flavor" {
  type = string
  default = "strawberry"
  }

source "null" "nullbuilder" {
  communicator = "none"
}

build {
  sources = ["source.null.nullbuilder"]
  provisioner "shell-local" {
    inline = ["echo ${var.flavor}"]
  }
  provisioner "shell-local" {
    inline = ["echo ${var.aws_access_key}"]
  }
  
}
