
packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

variable "topping" {
  type    = string
  default = "mushroom"
}

source "amazon-ebs" "skc_debian_11" {
  ami_name      = "skc_debian_11_base.0.0.0"
  instance_type = "t3.micro"
  source_ami    = "ami-0c7c4e3c6b4941f0f"
  ssh_username  = "admin"
  region        = "us-east-2"
  vpc_id =  "vpc-d7589ebc"
  subnet_id = "subnet-0484dd9608d0c5ba9"
  associate_public_ip_address = true
  tags = {
    Name = "skc-debian-11-base-0.0.0"
  }
}


build {
  sources = [
    "source.amazon-ebs.skc_debian_11"
  ]

  provisioner "shell" {
    #inline = ["sudo apt install -y python3"]
    inline = ["sudo apt-get update", "sudo apt-get upgrade -y", "python3 --version", "sudo apt install -y ansible"]
  }

  provisioner "ansible-local" {
    playbook_file   = "./ansible/build.yml"
    role_paths      = [
      "./ansible/roles/skc-print_os_release",
      "./ansible/roles/skc-add-users",
      "./ansible/roles/skc-install-tmux",
      "./ansible/roles/skc-install-emacs"
      ]
    extra_arguments = ["--extra-vars", "\"pizza_toppings=${var.topping}\""]
  }
}
